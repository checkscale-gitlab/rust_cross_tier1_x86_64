Dockerfile for Rust cross-compilation for 64 bit x86 architecture
=================================================================

This Dockerfile and the produced images cross-compile Rust programs 
for Linux, MS Win and MacOS.

The `container registry`_ includes the pre-build images. You can use the images directly with 

.. code-block:: bash

    docker run --rm -v $(pwd):/workdir -u $(id -u):$(id -g) registry.gitlab.com/dlab-dockers/rust_cross_tier1_x86_64:latest cargo build --target=x86_64-apple-darwin --release
    docker run --rm -v $(pwd):/workdir -u $(id -u):$(id -g) registry.gitlab.com/dlab-dockers/rust_cross_tier1_x86_64:latest cargo build --target=x86_64-pc-windows-gnu --release
    docker run --rm -v $(pwd):/workdir -u $(id -u):$(id -g) registry.gitlab.com/dlab-dockers/rust_cross_tier1_x86_64:latest cargo build --target=x86_64-unknown-linux-gnu --release

Images are tagged with "latest" and with the short git sha commit id used for 
the build. 

The `build_all.sh`_ script include these lines and builds binaries towards MacOS, MS Win and 
Linux 64 bit. 

To build the image manually download the Dockerfile and run

.. code-block:: bash

    docker build -t CHOOSE_A_NAME .

Only images for 64 bit architecture are provided. There are no plans currently 
to do 32 bit images, also considering that Rust starts to demote 32 bit support 
(`at least for MacOS`_).

.. _`build_all.sh`: https://gitlab.com/dlab-dockers/rust_cross_tier1_x86_64/blob/master/build_all.sh
.. _`container registry`: https://gitlab.com/dlab-dockers/rust_cross_tier1_x86_64/container_registry
.. _`at least for MacOS`: https://blog.rust-lang.org/2020/01/03/reducing-support-for-32-bit-apple-targets.html


Dependencies
------------

Docker, obviously

Why is this so complicated?
---------------------------------------------

Cross-compiling to different targets proofed to be quite a task.
In theory, it should be quite easy using rustup (e.g. see `this blog post`_ ).
In practice, however, it becomes quite complicated as soon as a project 
requires C bindings, for example when using a networking library relying on OpenSSL.

For Linux, this works mostly out of the box. For MacOS, the impressive `osxcross`_ does most of the heavy lifting.
In Windows you run into problems due to different case sensitivity settings 
of filenames. This can be solved by renaming some libraries. In addition, 
there is some bug with crt2 which needs to be fixed. As always, `Arch Wiki`_ 
was the place to find the solution.

See also `Related projects`_ for other approaches to solve the 
cross-compilation problem.


.. _`this blog post`: https://blog.rust-lang.org/2016/05/13/rustup.html
.. _`osxcross`: https://github.com/tpoechtrager/osxcross  
.. _`Arch Wiki`: https://wiki.archlinux.org/index.php/Rust#Cross_compiling



Tested libraries
----------------

The compile process was tested for


aho-corasick v0.7.6, 
autocfg v0.1.7, 
backtrace v0.3.40, 
backtrace-sys v0.1.32, 
base64 v0.10.1, 
bitflags v1.2.1, 
byteorder v1.3.2, 
bytes v0.4.12, 
cc v1.0.47, 
chrono v0.4.9, 
cookie v0.12.0, 
cookie_store v0.7.0, 
crc32fast v1.2.0, 
crossbeam-deque v0.7.2, 
crossbeam-epoch v0.8.0, 
crossbeam-utils v0.7.0, 
encoding_rs v0.8.20, 
error-chain v0.12.1, 
failure v0.1.6, 
failure_derive v0.1.6, 
flate2 v1.0.13, 
futures-cpupool v0.1.8, 
get_if_addrs v0.5.3, 
h2 v0.1.26, 
http v0.1.19, 
http-body v0.1.0, 
httparse v1.3.4, 
hyper v0.12.35, 
hyper-tls v0.3.2, 
idna v0.1.5, 
idna v0.2.0, 
indexmap v1.3.0, 
iovec v0.1.4, 
kernel32-sys v0.2.2, 
log v0.4.8, 
mac_address v1.0.2, 
maybe-uninit v2.0.0, 
memchr v2.2.1, 
memoffset v0.5.3, 
mime_guess v2.0.1, 
mio v0.6.19, 
native-tls v0.2.3, 
net2 v0.2.33, 
nix v0.15.0, 
num-integer v0.1.41, 
num-traits v0.2.9, 
num_cpus v1.11.1, 
openssl v0.10.25, 
openssl-sys v0.9.52, 
parking_lot v0.9.0, 
parking_lot_core v0.6.2, 
pkg-config v0.3.17, 
proc-macro2 v1.0.6, 
publicsuffix v1.5.4, 
quote v1.0.2, 
rand v0.6.5, 
rand_chacha v0.1.1, 
rand_os v0.1.3, 
rand_pcg v0.1.2, 
raw-cpuid v7.0.3, 
regex v1.3.1, 
reqwest v0.9.22, 
rustc_version v0.2.3, 
ryu v1.0.2, 
semver v0.9.0, 
semver-parser v0.7.0, 
serde v1.0.102, 
serde_derive v1.0.102, 
serde_json v1.0.41, 
serde_urlencoded v0.5.5, 
smallvec v0.6.13, 
string v0.2.1, 
syn v1.0.8, 
synstructure v0.12.3, 
sys-info v0.5.8, 
thread-id v2.0.0, 
thread_local v0.2.7, 
time v0.1.42, 
tokio v0.1.22, 
tokio-buf v0.1.1, 
tokio-io v0.1.12, 
tokio-reactor v0.1.10, 
tokio-tcp v0.1.3, 
tokio-threadpool v0.1.16, 
tzdata v0.4.1, 
unicase v2.6.0, 
unicode-normalization v0.1.9, 
unicode-xid v0.2.0, 
url v2.1.0, 
uuid v0.7.4, 
version_check v0.9.1, 
want v0.2.0, 
which v3.1.0, 
winapi-build v0.1.1;

Contributing
------------

The project if open for contributions, please discuss any issues first at the 
`issue tracker`_ (preferred) or via twitter (`@kst_stadler`_).

.. _`@kst_stadler`: https://twitter.com/kst_stadler
.. _`issue tracker`: https://gitlab.com/dlab-dockers/rust_cross_tier1_x86_64/issues

Acknowledgments
---------------

The project started after a `question`_ I posted to the Rust Twitter community.
I got a couple of great suggestions, many thanks to all. Special thanks to 
`James Waples`_ (`@jam_waffles`_ on Twitter) who pointed me in the right direction.

.. _`question`: https://twitter.com/kst_stadler/status/1200353739619602432
.. _`James Waples`: https://wapl.es/
.. _`@jam_waffles`: https://twitter.com/jam_waffles

Related projects
----------------

Cross_
    The Cross_ project aims to provide a pain-free compilation process for multiple 
    targets. Although I could not get it working for Windows (for the items in 
    `Tested libraries`_ ) and is only compiles to Mac on a Mac. However, it worked quite well 
    for compiling for the arm architecture (e.g. Raspberry Pi): 

    .. code-block:: bash

        cross build --target=armv7-unknown-linux-gnueabihf --release

rust_musl_docker_
    If you need a fully statically linked linux binary (thus build with MUSL_), the rust_musl_docker_ 
    project has a great image which also works with OpenSSL (thus for example 
    it works for Diesel and Rocket crates). To use is run:

    .. code-block:: bash

        docker run --rm -v $(pwd):/workdir registry.gitlab.com/rust_musl_docker/image:stable-1.39.0  cargo build --release -vv --target=x86_64-unknown-linux-musl


.. _Cross: https://github.com/rust-embedded/cross
.. _rust_musl_docker: https://gitlab.com/rust_musl_docker                                                                        
.. _MUSL: https://doc.rust-lang.org/edition-guide/rust-2018/platform-and-target-support/musl-support-for-fully-static-binaries.html
