FROM debian:buster

MAINTAINER Konstantin Stadler (https://gitlab.com/konstantinstadler)

# based on debian
RUN apt-get update && \
    apt-get install --yes \
        autoconf \
        binutils \
        build-essential \
        librust-winapi-dev \
        bzip2 \
        ca-certificates \
        clang \
        cmake \
        curl \
        file \
        g++-mingw-w64-x86-64 \
        gcc-arm-linux-gnueabihf \
        gdb-mingw-w64 \
        git \
        libc6-dev \
        libgmp-dev \
        libmpc-dev \
        libmpfr-dev \
        libsdl2-dev \
        libssl-dev \ 
        libtool \
        libxml2-dev \
        libz-mingw-w64-dev \
        m4 \
        make \
        pkg-config \
        wget \
        zlib1g-dev \
    && apt-get clean

# Install Rust and toolchains
# ---------------------------

ENV CARGO_HOME=/usr/local/rust
ENV RUSTUP_HOME=/usr/local/rustup

RUN curl --proto '=https' --tlsv1.2 -sSf https://sh.rustup.rs | sh -s -- -y \
  && /usr/local/rust/bin/rustup install stable \
  && /usr/local/rust/bin/rustup default stable \
  && /usr/local/rust/bin/rustup target install x86_64-unknown-linux-gnu \
  && /usr/local/rust/bin/rustup target install x86_64-pc-windows-gnu \
  && /usr/local/rust/bin/rustup target install x86_64-apple-darwin 

ENV PATH=$PATH:$CARGO_HOME/bin

# Linux gnu compilation setup
# ----------------------------

ENV CC_x86_64_unknown_linux_gnu=cc
ENV CXX_x86_64_unknown_linux_gnu=c++

RUN echo '[target.x86_64-unknown-linux-gnu] \n\
    linker = "cc"\n\
    ar = "ar"\n\n' >> /usr/local/rust/config

# MacOS compilation setup
# ------------------------

RUN cd /tmp && \
    git clone https://github.com/tpoechtrager/osxcross && \
    cd osxcross && \
    wget -nc https://s3.dockerproject.org/darwin/v2/MacOSX10.10.sdk.tar.xz && \
    mv MacOSX10.10.sdk.tar.xz tarballs/ && \
    UNATTENDED=yes OSX_VERSION_MIN=10.7 ./build.sh 

ENV PATH=$PATH:/tmp/osxcross/target/bin

ENV LIBZ_SYS_STATIC=1
ENV CC_x86_64_apple_darwin=o64-clang
ENV CXX_x86_64_apple_darwin=o64-clang++

RUN echo '[target.x86_64-apple-darwin] \n\
    linker = "x86_64-apple-darwin14-clang" \n\
    ar = "x86_64-apple-darwin14-ar" \n\n' >> /usr/local/rust/config

# MS Windows compilation setup
# ----------------------------

ENV CC_x86_64_pc_windows_gnu=x86_64-w64-mingw32-gcc
ENV CXX_x86_64_pc_windows_gnu=x86_64-w64-mingw32-g++

RUN echo '[target.x86_64-pc-windows-gnu] \n\
    linker = "x86_64-w64-mingw32-gcc"\n\
    ar = "x86_64-w64-mingw32-gcc-ar"\n\n' >> /usr/local/rust/config

# Bug fix 1: fix issue with lower/upper case lib names
RUN cp /usr/x86_64-w64-mingw32/lib/libsecur32.a /usr/x86_64-w64-mingw32/lib/libSecur32.a
RUN cp /usr/x86_64-w64-mingw32/lib/libiphlpapi.a /usr/x86_64-w64-mingw32/lib/libIphlpapi.a

# Bug fix 2: fix issue with crt2 and other other libraries
# based on https://wiki.archlinux.org/index.php/Rust#Cross_compiling
RUN for lib in crt2.o dllcrt2.o libmsvcrt.a; \
    do cp -v /usr/x86_64-w64-mingw32/lib/$lib \
             /usr/local/rustup/toolchains/stable-x86_64-unknown-linux-gnu/lib/rustlib/x86_64-pc-windows-gnu/lib/; \
    done

WORKDIR /workdir

# Allow non-root users access to the rust setup
RUN chmod -R 777 $CARGO_HOME
RUN chmod -R 777 $RUSTUP_HOME
RUN chmod -R 777 /workdir

# Provide some username assuming we run as UID 1000 from the docker run
RUN useradd -r -u 1000 SpaceJockey
USER SpaceJockey

CMD /bin/bash
