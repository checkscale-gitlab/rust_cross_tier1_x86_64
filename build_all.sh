#!/bin/bash

# An example script for the use rust-cross-tier1_x86_64 docker containers.
# This script is supposed to run in the root of a rust project (the place you 
# would normally call 'cargo build').
# It produces executables for Linux, MacOS and MS Win in the 'target' directory

set -e

# remove previous builds
#rm -rf ./target

# linux gnu - 64 bit 
docker run --rm \
           -v $(pwd):/workdir \
           -u $(id -u):$(id -g) \
           registry.gitlab.com/dlab-dockers/rust_cross_tier1_x86_64:latest \
           cargo build --target=x86_64-unknown-linux-gnu --release

## ms win - 64 bit
docker run --rm \
           -v $(pwd):/workdir \
           -u $(id -u):$(id -g) \
           registry.gitlab.com/dlab-dockers/rust_cross_tier1_x86_64:latest \
           cargo build --target=x86_64-pc-windows-gnu --release

## macos - 64 bit
docker run --rm \
           -v $(pwd):/workdir \
           -u $(id -u):$(id -g) \
           registry.gitlab.com/dlab-dockers/rust_cross_tier1_x86_64:latest \
           cargo build --target=x86_64-apple-darwin --release


# view into the container (root)
#docker run -it --rm -v $(pwd):/workdir registry.gitlab.com/dlab-dockers/rust_cross_tier1_x86_64:latest 

# view into the container (user 1000)
#docker run -it --rm -v $(pwd):/workdir -u $(id -u):$(id -g) registry.gitlab.com/dlab-dockers/rust_cross_tier1_x86_64:latest 

